import { requestJSON } from './net'

const PICARTO_URI = 'https://api.picarto.tv/v1/'

export class Channel {
  constructor () {
    this.id = ''
    this.name = ''
    this.avatar = ''
  }

  static fromJson (data) {
    const channel = new Channel()
    channel.id = data.user_id || data.id
    channel.name = data.name
    channel.avatar = data.avatar
    return channel
  }

  get avatarURI () {
    if (this.avatar) return this.avatar
    return 'https://picarto.tv/user_data/usrimg/' + encodeURIComponent(this.name.toLowerCase()) + '/dsdefault.jpg'
  }
}

export function getChannelByName (channelName) {
  return requestJSON(PICARTO_URI + 'channel/name/' + encodeURIComponent(channelName))
    .then(Channel.fromJson)
}

export function searchChannels (searchText) {
  searchText = searchText.trim()
  if (!searchText) return Promise.resolve([])
  return requestJSON(PICARTO_URI + 'search/channels?adult=true&q=' + encodeURIComponent(searchText))
    .then(data => {
      if (data.length) return data.map(Channel.fromJson)
      return getChannelByName(searchText)
        .then(channel => [Channel.fromJson(channel)])
        .catch(() => Promise.resolve([]))
    })
}
