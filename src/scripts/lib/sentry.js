import Vue from 'vue'
import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'

const SENTRY_DSN = process.env.SENTRY_DSN
const GIT_COMMIT_HASH = process.env.CI_COMMIT_SHA
const IS_PRODUCTION = process.env.NODE_ENV === 'production'

try {
  if (SENTRY_DSN && !Raven.isSetup()) {
    Raven
      .config(SENTRY_DSN)
      .addPlugin(RavenVue, Vue)
      .install()
      .setRelease(GIT_COMMIT_HASH)
      .setEnvironment(IS_PRODUCTION ? 'production' : 'development')
  }

  if (SENTRY_DSN) {
    console.info('Sentry error collection is enabled.')
  } else {
    console.info('Sentry error collection is disabled.')
  }
} catch (e) {}
