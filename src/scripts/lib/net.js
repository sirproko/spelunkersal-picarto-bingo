const oldThen = Promise.prototype.then

// eslint-disable-next-line no-extend-native
Promise.prototype.then = function () {
  const p = oldThen.apply(this, arguments)
  p.myCancellationHandler = () => this.myCancel()
  return p
}

// eslint-disable-next-line no-extend-native
Promise.prototype.myCancel = function () {
  if (this.myCancellationHandler) this.myCancellationHandler()
}

export function requestJSON (uri, method = 'GET', data) {
  const xhr = new XMLHttpRequest()
  let _reject

  const p = new Promise((resolve, reject) => {
    _reject = reject
    xhr.open(method, uri, true)
    xhr.onreadystatechange = () => {
      if (xhr.readyState !== XMLHttpRequest.DONE) return
      if (xhr.status >= 200 && xhr.status < 300) {
        let response = xhr.responseText
        if (xhr.status === 204 || !response) return resolve()
        try {
          response = JSON.parse(response)
        } catch (e) { return reject(e) }
        return resolve(response)
      } else {
        let response = xhr.responseText
        try {
          response = JSON.parse(response)
        } catch (_) {}
        reject(response)
      }
    }

    if (data) {
      xhr.setRequestHeader('Content-Type', 'application/json')
      xhr.send(JSON.stringify(data))
    } else {
      xhr.send()
    }
  })

  p.myCancellationHandler = () => { _reject('cancelled'); xhr.abort() }
  return p
}

export function timeout (timeoutDuration) {
  return new Promise(resolve => setTimeout(resolve, timeoutDuration))
}
