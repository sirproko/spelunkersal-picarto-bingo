const timeoutIds = {}

export function repeatableTimeout (timeoutName, callback, delay) {
  if (timeoutIds[timeoutName]) {
    clearTimeout(timeoutIds[timeoutName])
  }

  timeoutIds[timeoutName] = setTimeout(callback, delay)
}

export function clearRepeatableTimeout (timeoutName) {
  if (timeoutIds[timeoutName]) {
    clearTimeout(timeoutIds[timeoutName])
  }

  delete timeoutIds[timeoutName]
}
