/**
 * Returns an array over chunk_size elements of the slice at a time. The chunks are slices and do not overlap.
 * If chunk_size does not divide the length of the array, then the last array will not have length chunk_size.
 * @param array
 * @param chunkSize
 */
export function chunks (array, chunkSize) {
  const chunks = []

  for (let i = 0, c = array.length / chunkSize; i < c; ++i) {
    const chunk = array.slice(i * chunkSize, (i + 1) * chunkSize)
    chunk.chunkId = i
    chunks.push(chunk)
  }

  return chunks
}

export function columns (array, arrayWidth) {
  const rows = []

  for (let i = 0; i < arrayWidth; ++i) {
    const row = []
    for (let j = 0; j < arrayWidth; ++j) {
      row.push(array[j * arrayWidth + i])
    }
    rows.push(row)
  }

  return rows
}

export function diagonals (array, width) {
  const x1 = []
  const x2 = []

  for (let i = 0, c = width; i < c; ++i) {
    const j = width - i - 1
    x1.push(array[i * width + i])
    x2.push(array[i * width + j])
  }

  return [x1, x2]
}

/**
 *
 * @param a Array
 * @param b Array
 */
export function hasMissing (a, b) {
  for (let i = 0, c = b.length; i < c; ++i) {
    if (a.indexOf(b[i]) < 0) return true
  }
  return false
}
