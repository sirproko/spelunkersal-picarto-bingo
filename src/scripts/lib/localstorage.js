/**
 * This module is a thin wrapper to help manage LocalStorage, if available
 */

/**
 * Checks if the given Storage feature is present and available (i.e. not disabled by browser).
 * From https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API#Testing_for_availability
 * @param {string} type The storage type
 * @returns {boolean} If the given storage is available or not
 */
function storageAvailable (type) {
  const storage = window[type]
  try {
    const x = '__storage_test__'
    storage.setItem(x, x)
    storage.removeItem(x)
    return true
  } catch (e) {
    return e instanceof DOMException && (
      // everything except Firefox
      e.code === 22 ||
      // Firefox
      e.code === 1014 ||
      // test name field too, because code might not be present
      // everything except Firefox
      e.name === 'QuotaExceededError' ||
      // Firefox
      e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
      // acknowledge QuotaExceededError only if there's something already stored
      storage.length !== 0
  }
}

/**
 * If the local storage feature is available or not.
 * @type {boolean}
 */
const LOCAL_STORAGE_AVAILABLE = storageAvailable('localStorage')
const LOCAL_STORAGE_PREFIX = 'picarto_bingo/'

/**
 * A map containing event listeners for each listened item
 * @type {{}}
 */
const eventListeners = {}

if (LOCAL_STORAGE_AVAILABLE) {
  window.addEventListener('storage', (e) => {
    const listeners = eventListeners[e.key]
    if (!listeners) return
    for (const listener of listeners) {
      listener(e)
    }
  })
}

/**
 * Retrieves an item from the browser's local storage.
 * If the key does not exist, or if local storage is not available, the given defaultValue is returned.
 * @param {string} key
 * @param {*} defaultValue
 * @returns {*}
 */
export function get (key, defaultValue) {
  if (!LOCAL_STORAGE_AVAILABLE) return defaultValue
  const value = localStorage.getItem(LOCAL_STORAGE_PREFIX + key)
  if (value === null) return defaultValue
  return value
}

/**
 * Puts an item into the browser's LocalStorage.
 * This method fails silently if local storage is not available, or if the setItem() method failed.
 * @param {string} key
 * @param {string} value
 */
export function set (key, value) {
  if (!LOCAL_STORAGE_AVAILABLE) return
  try {
    localStorage.setItem(LOCAL_STORAGE_PREFIX + key, value)
  } catch (e) {}
}

/**
 * Watches a specific local storage key for events
 * @param key
 * @param {LocalStorageCallback} callback
 */
export function watch (key, callback) {
  if (!LOCAL_STORAGE_AVAILABLE) return
  let listeners = eventListeners[LOCAL_STORAGE_PREFIX + key]
  if (!listeners) {
    listeners = [callback]
    eventListeners[LOCAL_STORAGE_PREFIX + key] = listeners
  } else {
    listeners.push(callback)
  }
}

/**
 * @callback LocalStorageCallback
 * @param {StorageEvent} event
 */
