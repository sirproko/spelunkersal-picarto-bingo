import Vuex from 'vuex'
import { Grid } from './data/grid'
import { Channel } from './lib/picarto'
import GridData from '../data'
import * as LocalStorage from './lib/localstorage'

export const store = new Vuex.Store({
  state: {
    /**
     *
     * @returns {Grid}
     */
    get grid () {
      return this.grids[this.currentGrid]
    },
    /**
     * @type Array<Grid>
     */
    grids: [],
    /**
     * @type Number|null
     */
    currentGrid: null,
    /**
     * @type Boolean
     */
    initialized: false,
    /**
     * @type Object<Channel>
     */
    channelCache: {},
    /**
     * @type Array<Number>
     */
    recentlyUsed: []
  },
  mutations: {
    init (state) {
      const storedStateString = LocalStorage.get('state', null)
      if (!storedStateString) {
        state.grids = [Grid.fromJson(GridData)]
        state.currentGrid = 0
        state.initialized = true
        return
      }
      const storedState = JSON.parse(storedStateString)

      for (const id in storedState.channelCache) {
        if (storedState.channelCache.hasOwnProperty(id)) state.channelCache[id] = Channel.fromJson(storedState.channelCache[id])
      }
      state.grids = storedState.grids.map(Grid.fromStore)
      state.recentlyUsed = storedState.recentlyUsed
      state.initialized = true
      if (state.currentGrid === null) state.currentGrid = state.grids.length - 1
    },
    cacheChannel (state, { channel }) { state.channelCache[channel.id] = channel },
    addChannelToCell (state, { channel, cellId }) {
      let knownChannel = this.getters.findCachedChannel(channel.id)
      if (!knownChannel) {
        this.commit('cacheChannel', { channel })
      }
      state.grid.findCellById(cellId).addChannel(channel.id)
    },
    removeChannelFromCell (state, { channel, cellId }) {
      state.grid.findCellById(cellId).removeChannel(channel.id)
    },
    addToRecentlyUsed (state, { channels }) {
      channels.forEach(id => {
        const existingId = state.recentlyUsed.indexOf(id)
        if (existingId >= 0) state.recentlyUsed.splice(existingId, 1)
        state.recentlyUsed.unshift(id)
      })
    },
    newGrid (state) {
      state.grids.push(Grid.fromJson(GridData))
    },
    setCurrentGrid (state, { gridId }) {
      if (gridId < state.grids.length) state.currentGrid = gridId
    },
    deleteGrid (state, { gridId }) {
      if (state.grids.length <= 1) return
      if (state.currentGrid >= state.grids.length - 1) state.currentGrid = state.grids.length - 2
      state.grids.splice(gridId, 1)
    },
    updateBingos (state) {
      state.grid.previousBingos = state.grid.bingos
    }
  },
  getters: {
    findCachedChannel: state => id => state.channelCache[id],
    savedState: state => ({
      grids: state.grids.map(g => g.saveState()),
      channelCache: state.channelCache,
      recentlyUsed: state.recentlyUsed
    })
  }
})

store.subscribe((mutation, state) => {
  if (mutation.type === 'init') return
  LocalStorage.set('state', JSON.stringify(store.getters.savedState))
})

LocalStorage.watch('state', () => store.commit('init'))
