import './lib/sentry'
import 'es6-promise/auto'
import Vue from 'vue'
import Vuex from 'vuex'

import '../../node_modules/typeface-open-sans/index.css'
import '../styles/main.scss'
import Main from './main.vue'

Vue.use(Vuex)

const { store } = require('./store')

// eslint-disable-next-line no-new
new Vue({
  el: document.getElementById('content'),
  render: h => h(Main),
  store
})
