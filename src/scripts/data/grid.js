import { Cell } from './cell'
import { chunks, columns, diagonals } from '../lib/array'
import GridData from '../../data'

export const GRID_SIZE = 5

export class Grid {
  constructor () {
    const now = new Date()
    this.name = `Bingo (${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()})`
    /**
     * @type {Array<Cell>}
     */
    this.cells = []
    this.previousBingos = null
  }

  get rows () {
    return chunks(this.cells, GRID_SIZE)
  }

  get columns () {
    return columns(this.cells, GRID_SIZE)
  }

  get diagonals () {
    return diagonals(this.cells, GRID_SIZE)
  }

  get bingos () {
    const bingos = []

    this.rows.forEach((row, i) => {
      if (Grid.isBingo(row)) bingos.push('r' + i)
    })

    this.columns.forEach((row, i) => {
      if (Grid.isBingo(row)) bingos.push('c' + i)
    })

    this.diagonals.forEach((row, i) => {
      if (Grid.isBingo(row)) bingos.push('x' + i)
    })

    return bingos
  }

  /**
   *
   * @param cells Array<Cell>
   */
  static isBingo (cells) {
    for (let i = 0, c = cells.length; i < c; ++i) {
      if (!cells[i].channelHits.length) return false
    }
    return true
  }

  get hitCount () {
    return this.cells.reduce((acc, cell) => acc + cell.channelHits.length, 0)
  }

  /**
   * @param id
   * @returns {Cell | undefined}
   */
  findCellById (id) {
    return this.cells.find(cell => cell.id === id)
  }

  assign (other) {
    this.cells = other.cells.map(Cell.fromJson)
  }

  static fromJson (data) {
    if (!Array.isArray(data.cells)) {
      throw new Error('Invalid data : cells must be an array')
    }

    const grid = new Grid()
    grid.cells = data.cells.map(Cell.fromJson)

    return grid
  }

  saveState () {
    return {
      name: this.name,
      cells: this.cells
        .filter(cell => !!cell.channelHits.length)
        .reduce((acc, cell) => { acc[cell.id] = cell.channelHits; return acc }, {})
    }
  }

  loadFromState (gridState) {
    const cells = gridState.cells
    this.name = gridState.name

    for (const cellId in cells) {
      if (cells.hasOwnProperty(cellId)) {
        const cell = this.findCellById(cellId)
        if (cell) cell.channelHits = cells[cellId]
      }
    }

    this.previousBingos = this.bingos
  }

  static fromStore (storedGrid) {
    const grid = Grid.fromJson(GridData)
    grid.loadFromState(storedGrid)
    return grid
  }
}
