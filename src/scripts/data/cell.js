export const GRID_SIZE = 5

export class Cell {
  constructor () {
    this.title = ''
    this.subtitle = ''
    this.position = null
    this.id = null
    this.size = ''
    this.channelHits = []
  }

  get x () {
    return this.position % GRID_SIZE
  }

  get y () {
    return Math.floor(this.position / GRID_SIZE)
  }

  get channels () {
    return this.channelHits.map(c => c.id)
  }

  hasChannel (id) {
    return this.channelHits.findIndex(c => c.id === id) > -1
  }

  addChannel (id) {
    if (this.hasChannel(id)) return
    this.channelHits.push({ id, time: Math.floor(Date.now() / 1000) })
  }

  removeChannel (id) {
    const index = this.channelHits.findIndex(c => c.id === id)
    if (index < 0) return
    this.channelHits.splice(index, 1)
  }

  static fromJson (data, cellId) {
    const cell = new Cell()
    cell.title = data.title
    cell.subtitle = data.subtitle
    cell.id = data.id
    cell.size = data.size || ''
    cell.position = cellId
    if (Array.isArray(data.channelHits)) cell.channelHits = cell.channels.concat(data.channelHits)
    return cell
  }
}
